# Документация к API в Postman:
1) Оформление новой задачи (заказа): https://documenter.getpostman.com/view/18111098/UVByHq47
2) Оформление нового отзыва: https://documenter.getpostman.com/view/18128199/UVByKWJu
3) Оформление нового пользователя Django: https://documenter.getpostman.com/view/18128199/UVByKWPQ
После реализации новых фичей и тестирования их в Postman, просьба выкладывать документации к ним сюда.