from django.db import models
from django.contrib.auth.models import User

from fl_app.validators import validate_mark

"""
Unique path to the user file save directory
"""
def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.customer_id, filename)


class Role(models.Model):
    # role_id = models.ForeignKey(User, on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True)
    name_role = models.CharField(max_length=50)

    class Meta():
        verbose_name = 'Роль'
        verbose_name_plural = 'Роли'

    def __str__(self):
        return self.name_role


class Level(models.Model):
    # level_id = models.ForeignKey(Task, on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True)
    name_level = models.CharField(max_length=50)

    class Meta():
        verbose_name = 'Уровень'
        verbose_name_plural = 'Уровни'

    def __str__(self):
        return self.name_level


class Status(models.Model):
    # status_id = models.ForeignKey(OrderStatus, on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True)
    name_status = models.CharField(max_length=50)

    class Meta():
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'

    def __str__(self):
        return self.name_status
    

class Category(models.Model):
    # category_id = models.ForeignKey(Task, on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True)
    name_category = models.CharField(max_length=50)

    class Meta():
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name_category


class PaymentType(models.Model):
    name = models.CharField(max_length=50)
    describe = models.TextField(blank=True)
    
    class Meta():
        verbose_name = 'Типы платежа'
        verbose_name_plural = 'Типы платежа'
    
    def __str__(self):
        return self.name


class Task(models.Model):
    # task_id = models.ForeignKey(User, on_delete=models.CASCADE)
    id = models.AutoField(primary_key=True)
    category_id = models.ForeignKey(Category, on_delete=models.CASCADE)
    level_id = models.ForeignKey(Level, on_delete=models.CASCADE)
    customer_id = models.ForeignKey(User, on_delete=models.CASCADE)
    

    title_task = models.CharField(max_length=70)
    price_task = models.IntegerField()
    description_task = models.TextField()
    upload = models.FileField(upload_to=user_directory_path, default="")
    # date_task = models.DateTimeField()

    class Meta():
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'

    def __str__(self):
        return self.title_task


class Quality(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    task_id = models.ForeignKey(Task, on_delete=models.CASCADE)
    comment = models.TextField(default='')
    mark = models.IntegerField(validators=[validate_mark])

    class Meta():
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

    def __str__(self):
        return self.comment


class Order(models.Model):
    id = models.AutoField(primary_key=True)
    task_id = models.OneToOneField(Task, on_delete=models.CASCADE)
    executor_id = models.ForeignKey(User, on_delete=models.CASCADE)

    date_start_order = models.DateTimeField()
    date_end_order = models.DateTimeField()
    payment_type_order = models.CharField(max_length=50)
    price_order = models.IntegerField()

    class Meta():
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return str(self.id)
        
    

class FeedBack(models.Model):
    # id_fb = models.ForeignKey()
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    task_id = models.ForeignKey(Task, on_delete=models.CASCADE)
    comment_fb = models.TextField()
    # price_fb = models.IntegerField() # зачем цена в фидбеке?

    class Meta():
        verbose_name = 'Обратная связь'
        verbose_name_plural = 'Обратные связи'

    def __str__(self):
        return self.comment_fb


class OrderStatus(models.Model):
    id = models.AutoField(primary_key=True)
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE)

    date_start_ordstatus = models.DateTimeField()
    date_end_ordstatus = models.DateTimeField()

    class Meta():
        verbose_name = 'Статус заявки'
        verbose_name_plural = 'Статусы заявок'

    def __str__(self):
        return str(self.order_id)
