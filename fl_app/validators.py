from rest_framework import serializers


def validate_mark(value):
    if not ((value > 0) and (value <= 5)):
        raise serializers.ValidationError('Оценка заказа должна быть от 0 до 5 баллов.')
