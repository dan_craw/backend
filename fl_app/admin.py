from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

# # Register your models here.
@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'date_start_order', 'date_end_order', 'payment_type_order', 'price_order']

@admin.register(OrderStatus)
class OrderStatusAdmin(admin.ModelAdmin):
    list_display = ['id', 'order_id', 'status_id', 'date_start_ordstatus', 'date_end_ordstatus']

@admin.register(FeedBack)
class FeedBackAdmin(admin.ModelAdmin):
    list_display = ['id', 'user_id', 'task_id', 'comment_fb']

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ['id', 'category_id', 'level_id', 'customer_id', 'title_task', 'price_task', 'description_task']

@admin.register(Role)
class RoleAdmin(admin.ModelAdmin): 
    list_display = ['id', 'name_role']

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name_category']

@admin.register(Level)
class LevelAdmin(admin.ModelAdmin):
    list_display = ['id', 'name_level']

@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ['id', 'name_status']

@admin.register(Quality)
class QualityAdmin(admin.ModelAdmin):
    list_display = ['id', 'user_id', 'task_id', 'comment', 'mark']