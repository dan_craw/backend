
from rest_framework import generics
from fl_app.api.serializers import FeedBackSerializer
from fl_app.models import FeedBack
from fl_app.api.serializers import PaymentTypeSerializer
from fl_app.models import PaymentType
from fl_app.api.serializers import OrderSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from fl_app.models import Order

class OrderListView(generics.ListCreateAPIView):
    
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
        


class OrderDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class PaymentTypeListView(generics.ListCreateAPIView):
    
    queryset = PaymentType.objects.all()
    serializer_class = PaymentTypeSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
        


class PaymentTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = PaymentType.objects.all()
    serializer_class = PaymentTypeSerializer


class FeedBackListView(generics.ListCreateAPIView):
    
    queryset = FeedBack.objects.all()
    serializer_class = FeedBackSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
        


class FeedBackDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = FeedBack.objects.all()
    serializer_class = FeedBackSerializer


