from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView


class UserView(APIView):

    """
    Save new User (Django) to database
    Arguments for create new User: login, email, password
    """
    def post(self, request):
        login = request.data.get('login')
        email = request.data.get('email')
        password = request.data.get('password')
        if not (email.strip() or password.strip()):
            return Response(status=401)
        try:
            if User.objects.filter(email__iexact=email).exists():
                raise ValidationError('User with this email already exist')
            user = User.objects.create_user(login, email, password)
            return Response(status=201)
        except Exception:
            return Response(status=401)
