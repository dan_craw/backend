from rest_framework.views import APIView
from rest_framework.response import Response
from fl_app.api.serializers import QualitySerializer
from fl_app.models import Quality


class QualityView(APIView):

    """
    Output Quality list (for actual qualities)
    Arguments for the address bar for filter: id (primary key of Quality model)
    """
    def get(self, request):

        quality_id = self.request.query_params.get('id')
        if quality_id:
            quality = Quality.objects.filter(id=quality_id)
        else:
            quality = Quality.objects.all()

        serializer = QualitySerializer(quality, many=True)
        return Response(serializer.data)

    """
    Save new Quality to database
    Arguments for create new Quality: user_id, task_id, comment (optional), mark (from 1 to 5)
    """
    def post(self, request):
        quality = QualitySerializer(data=request.data)
        if quality.is_valid():
            quality.save()
            return Response(status=201)
        else:
            return Response(status=400)
