from rest_framework.views import APIView
from rest_framework.response import Response 

from fl_app.models import Task, Level, Category
from fl_app.api.serializers import TaskListSerializer, LevelsSerializer, CategorySerializer

from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework import status


class TaskListView(APIView):   

    parser_classes = (MultiPartParser, FormParser)

    """ 
    Output Task list (for actual tasks page)
    Arguments for the address bar for filter: cat_id, lev_id
    """
    def get(self, request):

        cat_id = self.request.query_params.get('cat_id', None)
        lev_id = self.request.query_params.get('lev_id', None)

        if cat_id:
            tasks = Task.objects.filter(category_id = cat_id)
        elif lev_id:
            tasks = Task.objects.filter(level_id = lev_id)
        elif cat_id and lev_id:
            tasks = Task.objects.filter(category_id = cat_id, level_id = lev_id)
        else:
            tasks = Task.objects.all()

        serializer = TaskListSerializer(tasks, many = True)
        return Response(serializer.data)


    """ 
    Save new task to database (create task from customer)
    """
    def post(self, request):
        task = TaskListSerializer(data = request.data)
        if task.is_valid():
            task.save()
            return Response(task.data, status=status.HTTP_201_CREATED)
        else:
            return Response(task.errors, status=status.HTTP_400_BAD_REQUEST)



class LevelsView(APIView):

    """ 
    Get list of Levels 
    (pk - for get level by id)
    """
    def get(self, request, pk=None):
        if pk:
            level = Level.objects.get(id = pk)
            serializer = LevelsSerializer(level)
        else:
            levels = Level.objects.all()
            serializer = LevelsSerializer(levels, many = True)
        return Response(serializer.data)


class CategoriesView(APIView):

    """ 
    Get categories list
    (pk - for get category by id)
    """
    def get(self, request, pk=None):
        if pk:
            category = Category.objects.get(id = pk)
            serializer = CategorySerializer(category)
        else:
            categories = Category.objects.all()
            serializer = CategorySerializer(categories, many = True)
        return Response(serializer.data)

