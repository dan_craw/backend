
from django.urls import path
from fl_app.api.views.orders import FeedBackDetail, FeedBackListView
from fl_app.api.views.orders import PaymentTypeDetail, PaymentTypeListView
from fl_app.api.views.orders import OrderDetail
from fl_app.api.views.orders import OrderListView


from fl_app.api.views.create_quality import QualityView
from fl_app.api.views.create_task_user import LevelsView, CategoriesView, TaskListView
from fl_app.api.views.create_user import UserView

urlpatterns = [
    
    # Get level list
    path('levels/', LevelsView.as_view(), name='levels_list'),
    path('levels/<int:pk>', LevelsView.as_view(), name='level_by_id'),

    # Get categories list
    path('categories/', CategoriesView.as_view(), name='categories_list'),
    path('categories/<int:pk>', CategoriesView.as_view(), name='category_by_id'),

    # For tasks (get tasks, create a new task)
    path('tasks/', TaskListView.as_view(), name='tasks'),
    path('orders/', OrderListView.as_view(), name='order_list'),
    path('orders/<int:pk>/', OrderDetail.as_view(), name='order_detail'),
    path('payment_types/', PaymentTypeListView.as_view(), name='payment_types_list'),
    path('payment_types/<int:pk>/', PaymentTypeDetail.as_view(), name='payment_types_detail'),
    path('feedback/', FeedBackListView.as_view(), name='feedback_list'),
    path('feedback/<int:pk>/', FeedBackDetail.as_view(), name='feedback_detail'),

    # For qualities
    path('quality/', QualityView.as_view(), name='quality'),

    # For create_user
    path('create_user/', UserView.as_view(), name='create_user'),
]