from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.response import Response 
from fl_chat.models import Message
from fl_chat.serializers import MessageSerializer, UserProfileSerializer
from rest_framework import generics
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated


class MessageListView(generics.ListCreateAPIView):
    model = Message
    serializer_class = MessageSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    def get_queryset(self):
        return Message.objects.filter(order_id = self.kwargs['order_id'])
        

class MessageDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class UserProfileListView(APIView):
    def get(self, request):
        users =  User.objects.all()
        serializer =  UserProfileSerializer(users, many = True)
        return Response(serializer.data)
