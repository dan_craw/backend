from django.contrib.auth.models import User
from django.db import models
from fl_app.models import Order
from django.conf import settings
import datetime

from django.core.cache import cache


class Message(models.Model):
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, related_name='order', blank=False)
    sender = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='sender', blank=False)
    message = models.CharField(max_length=1200, blank=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.message

    class Meta:
        ordering = ('timestamp',)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

    def last_seen(self):
        return cache.get('last_seen_%s' % self.user.username)

    def online(self):
        print(self.last_seen(), "*"*123)
        if self.last_seen():
            now = datetime.datetime.now()
            if now > (self.last_seen() + datetime.timedelta(seconds=settings.USER_ONLINE_TIMEOUT)):
                return False
            else:
                return True
        else:
            return False
