import json
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from fl_app.models import PaymentType
from fl_chat.models import Message
from requests.auth import HTTPBasicAuth
from fl_app.models import Order

class AccountTests(APITestCase):
    def setUp(self):
        user = User.objects.create_user('admin', 'admin')
        self.client.force_authenticate(user)
        payment_type = PaymentType.objects.create(name="Bitcoin", describe ='Bitcoin very good')
        Order.objects.create(
            date_start_order = "2021-11-01T14:17:00+03:00",
            date_end_order = "2021-11-01T14:17:00+03:00",
            price_order = 1221,
            payment_type_order=payment_type
            )

    def test_get_Messages(self):
        url = reverse('message_list', kwargs={'order_id': 1})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_post_Messages(self):
        url = reverse('message_list',kwargs={'order_id': 1})
        data = {'message': 'message from test', 'order':1, 'sender':1}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["message"], "message from test")
