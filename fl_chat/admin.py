from django.contrib import admin
from fl_app.models import Category
from fl_app.models import Task
from fl_app.models import PaymentType
from fl_app.models import Order, Level
from fl_chat.models import Message

# Register your models here.
# admin.site.register(Order)
admin.site.register(Message)
admin.site.register(PaymentType)
# admin.site.register(Task)
# admin.site.register(Category)
# admin.site.register(Level)