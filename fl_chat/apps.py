from django.apps import AppConfig


class FlChatConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fl_chat'
