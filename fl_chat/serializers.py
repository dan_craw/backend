from django.contrib.auth.models import User
from fl_app.models import Order
from rest_framework import serializers
from fl_chat.models import Message, UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    online = serializers.ReadOnlyField(source='userprofile.online')

    class Meta:
        model = User
        fields = ['id', 'username', 'online']



class MessageSerializer(serializers.ModelSerializer):
    order = serializers.SlugRelatedField(many=False, slug_field='id', queryset=Order.objects.all())
    sender = serializers.SlugRelatedField(many=False, slug_field='id', queryset=User.objects.all())
    display_sender = serializers.SerializerMethodField()
    def get_display_sender(self,msg):
        return UserProfileSerializer(msg.sender, many = False).data
    class Meta:
        model = Message
        fields = '__all__'
