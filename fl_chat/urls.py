from django.urls import path
from fl_chat.models import UserProfile

from fl_chat.views import MessageListView, MessageDetail, UserProfileListView



urlpatterns = [    
    path('<int:order_id>/messages/', MessageListView.as_view(), name='message_list'),
    path('<int:order_id>/messages/<int:pk>/', MessageDetail.as_view(), name='message_detail'),
    path('userprofiles/', UserProfileListView.as_view(), name='user_profile'),
]